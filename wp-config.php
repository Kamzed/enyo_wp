<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'enyo_db');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', '');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '~3a1mmB_=#j IFA_N$ )=Mr+@ks6*sn[*ZXbkmpV>h@yK+?Ao?5|ugD83vRs?`b%');
define('SECURE_AUTH_KEY',  'bI`P68-5cu|Xi^L!m)@iP1gvyRyC?ai>y??iX#i{;:/e=yqu:-8R(@A)nC:_mEU!');
define('LOGGED_IN_KEY',    '(r+W=Zpm8Jt1YkhHpc0me#c=>6,/{~ngbJ-230H=i*Pl?q90ZTZT<D;t8/-aQ2E;');
define('NONCE_KEY',        'MZ@U#WcgAbO,9/2W{fsQ?GssE_p>ePAn)H=[)tZ8lM!qOvMSE@H6,:*P%_ZLYi!x');
define('AUTH_SALT',        '63KKugF-wQ2+jn8A=<Ls2@vcuv9,^u23bkb`>_]T)s9-$|,zg1S} >/(Y.1.TN` ');
define('SECURE_AUTH_SALT', '&C0yV;tz8CG8DnNp?>BDX/-q&X]QA!&/QWPIu #a-A=;h52LT4uSEvrvJ?Ya7hPY');
define('LOGGED_IN_SALT',   'J)+nqjPy6<l;)FKj`~ ,@8YsP^-W&M-Ws9OJRB0k-DY-/?;3Fc$_rcB>}X-#508i');
define('NONCE_SALT',       'HJwO1xT()uho/j{H3!l{-9P9c*6wOH&X}W}]&`Cb#dywrWgjgp~PK,+*`AhtN,@C');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/*Что бы плагины работали и обновлялось все*/
define('FS_METHOD','direct');

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
